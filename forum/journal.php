<?php

define('H', $_SERVER['DOCUMENT_ROOT'].'/');

foreach (array('start', 'compress', 'sess', 'settings', 'db_connect', 'ipua', 'fnc', 'user') as $inc) {
    include_once H . 'sys/inc/' . $inc . '.php';
}

$set['title'] = 'Журнал форума';
include_once '../sys/inc/thead.php';
title().aut().only_reg();

if (!isset($_GET['all']) && !isset($_GET['new'])) {
	header('Location: journal.php?new');
	exit;
}

$isset = (isset($_GET['type'])) ? '?type='.htmlspecialchars($_GET['type']).'&amp;' : '?';
$type = (isset($_GET['all'])) ? 'all' : 'new';
$read = (isset($_GET['all'])) ? NULL : 'AND `read` = "0"';

?>
<table class = 'post'>
	<tr>
		<td class = 'au' style = 'width: 50%; text-align: center'>
			<?= (isset($_GET['all'])) ? 'Все' : '<a href = "journal.php'.$isset.'all" style="display: block;">Все</a>' ?>
		</td>
		<td class = 'au' style = 'width: 50%; text-align: center'>
			<?= (!isset($_GET['all'])) ? 'Новые' : '<a href = "journal.php'.$isset.'new" style="display: block;">Новые</a>' ?>
		</td>
	</tr>
</table>
<div class = 'foot'>
	<?= (isset($_GET['type'])) ? '<a href = "journal.php">Все</a>' : '<strong>Все</strong>' ?> | 
	<?= (isset($_GET['type']) && $_GET['type'] == 'my_themes') ? '<strong>Мои темы</strong>' : '<a href = "journal.php?type=my_themes&amp;'.$type.'">Мои темы</a>' ?> | 
	<?= (isset($_GET['type']) && $_GET['type'] == 'themes') ? '<strong>Темы</strong>' : '<a href = "journal.php?type=themes&amp;'.$type.'">Темы</a>' ?> | 
	<?= (isset($_GET['type']) && $_GET['type'] == 'answers') ? '<strong>Ответы</strong>' : '<a href = "journal.php?type=answers&amp;'.$type.'">Ответы</a>' ?> | 
	<?= (isset($_GET['type']) && $_GET['type'] == 'quotes') ? '<strong>Цитаты</strong>' : '<a href = "journal.php?type=quotes&amp;'.$type.'">Цитаты</a>' ?> | 
	<?= (isset($_GET['type']) && $_GET['type'] == 'privat') ? '<strong>Приват</strong>' : '<a href = "journal.php?type=privat&amp;'.$type.'">Приват</a>' ?>
</div>
<?

$type_j = (isset($_GET['type'])) ? '`type` = "'.mysql_real_escape_string($_GET['type']).'" AND' : NULL;
$k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `f_journal` WHERE '.$type_j.' `id_user` = '.$user['id'].' '.$read), 0);
$k_page = k_page($k_post, $set['p_str']);
$page = page($k_page);
$start = $set['p_str']*$page-$set['p_str'];
if ($k_post == 0) {
    ?>
    <div class = 'p_m'>
    	Записей нет.<br />
    	В журнале будут отображаться все ответы на Ваши комментарии и сообщения, оставленные пользователями в тех темах, за которыми Вы следите.
    </div>
    <?
} else {
	$js = mysql_query('SELECT * FROM `f_journal` WHERE '.$type_j.' `id_user` = '.$user['id'].' '.$read.' ORDER BY `id` DESC LIMIT '.$start.', '.$set['p_str']);
	while ($j = mysql_fetch_object($js)) {
		?>
		<div class = 'p_m'>
			<?= output_text($j->text) ?><br />
			<strong>Время отправки:</strong> <?= vremja($j->time) ?>
		</div>
		<?
	}
	if ($k_page > 1) {
        str('journal.php'.$isset.$type, $k_page, $page);
    }
    if (isset($_GET['new'])) {
		mysql_query('UPDATE `f_journal` SET `read` = "1" WHERE `id_user` = '.$user['id']);
	}
}

include_once '../sys/inc/tfoot.php';

?>